To start:
1. R&Rstudio are free, so you can easily download and install R.
2. You should download both R and Rstudio, where RStudio (https://rstudio.com) is more user-friendly way of using R (To download R, go to https://www.r-project.org/.).


Outline:

-   Set up Rstudio

-   Define objects: numbers, characters, logical values (Booleans), vectors, and data.frame

-   Dealing with missing values

-   Vector Indexing

-   Vector Operations

-   Vector Filtering

-   Define data.frame

-   Functions

-   Loops and if statement

-   Read data

-   Simple linear regression

-   Plot and save regression results


