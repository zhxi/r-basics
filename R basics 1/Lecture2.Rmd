---
title: "Introduction to R and RStudio"
output:
  pdf_document: default
  html_document: default
---

# **Outline:**

-   Set up Rstudio

-   Define objects: numbers, characters, logical values (Booleans), vectors, and data.frame

-   Dealing with missing values

-   Vector Indexing

-   Vector Operations

-   Vector Filtering

# R and Rstudio

Rstudio is an easier way to use R with many built in features. This tutorial will walk you through the basics of R and will give you the information needed to start coding in R.

## Creating new files

In the top left corner of RStudio you should see "File" and directly under that the "new file" button. Selecting "File" and hovering "New File" will reveal the same menu as the "new file" button. Or there is a shortcut on the second line of the top left corner ![](Rscript_shortcut.png){width="5%"}. Either method can be used to create a variety of file types, but the one you will use most commonly is the "R Script" which has a ".R" file ending.

## Saving an R script

You can save an R script in a few ways. One way is to select 'File \> Save' or 'File \> Save as' in the top-left corner of RStudio. Another way is to select the floppy disk button ![](save_shortcut.png){width="4%"} on top of the R script. A final way is to use the keyboard shortcut "Ctrl+S" for Windows and Linux, or "Cmd+S" on Mac.

**For Assignment 2**, you will be asked to submit a "R Script" to show all your codes for each question. Please also include "comments" to help me identify which question your codes are answering to.

## Layout of Rstudio

After creating a new R script, let's look at your screen. Your screen now is partitioned into 4 parts: 1 The top left part of RStudio is the R script part for you to write and store your codes; 2 The bottom left part of RStudio is the R Console part, where it will display the results of your codes; 3 The top right part has four options: Environment, History, Connections and Tutorial. Environment is the option that we need to use more often, where it lists the variables and functions present in the current R session; 4 The bottom right part has five options: Files, Plots, Packages, Help and Viewer. A plot will show up, under "Plots", after you create one in your codes. And when you ask R for help, under "Help", R will present a R Documentation, which serves as commentary on how the software works, includes use cases, and cites any relevant sources.

## Comments

Let's start writing on R script! Can you tell the difference between the two lines below?

```{r}
2+2
# 2+2 
```

You should notice that only the first line resulted in what you may have expected: 4. This is because the presence of a \# indicates a comment, and R will skip comments.

It is a good practice to always comment your codes. If you work with a team, commenting will be necessary to understand and build upon each other's code. You also never know when you'll need to reference an old file, and having those comments will save you a lot of time.

### Sectioning your codes

Comments can also be used to section your codes. Especially when you have a long list of codes (e.g 2000 lines), sectioning will be super important to help trace back to the codes you want. To section, start with any number of pound signs (\#) and ends with four or more -, =, or \# characters.

## Defining objects

Defining objects works a lot like defining variables in a math equation. We can assign a value to an object, using the "\<-" operator, and then the object will take on the properties of that value. "\<-", the arrow, also indicates a direction, defining the value on RHS to the object on LHS. Therefore, you can also use "-\>" to assign value on LHS to the object on RHS. Try the followings:

```{r}
myfirstobject <- "Hello"
print(myfirstobject)

```

~*Note:You can include alphabet, number, dot and underline in object names.*~

~*However, you should only start a name with alphabet.*~


```{r}
"Hello" -> mysecondobject
print(mysecondobject)
```

~*Note:You can use "=" sign to define objects in R, and it works the same as "\<-" in default.*~

~*However, it's not recommended to use "=" since it specifies\ no\ direction\ and\ may generate issues sometime.*~

### Common issues when defining objects

Let's look at some common issues when defining objects.

```{r error=TRUE}
myfirstobject <- Hello
```

You got your first R error! Why? An object must be defined and stored in the current environment before assigning it to other objects. Here in our example, Hello is never defined in R, so we cannot assign it to myfirstobject. Tip: You can always check the present R "Environment" on the top right of RStudio, where all objects and functions defined are listed.

We can get rid of the error by defining Hello first.

```{r}
Hello <- "Hola"
mysecondobject <- Hello
mysecondobject
```

~*Note: you can view the contents of an object by typing its name, as well as using print().*~

## Running your codes

Direct your attention to the upper right corner of R script, you should see the button "Run" ![](run_shortcut.png){width="8%"}. You can run your code by highlighting or selecting the lines you wish to run and then pressing this button. Alternatively, select the desired lines in the same manner and then use the keyboard shortcut: "Ctrl + Enter" for Windows and Linux, or "Cmd + Enter" for Mac.

## Data Types

The most frequently used data types in R include numbers, characters (equivalent to strings in R), Booleans (can also be called logical values: True and False), vectors and then data.frame.

There is a hierarchy in these data types: for the basic layer, we have numbers, characters, and Booleans; Above these, we have vectors, which we can use to combine the same type of data; On the very top, we have data.frame, which we can use to combine different types of vectors. In this course, we will dabble all data types to give you a general idea about how to deal with each of them.

In any programming language, different types of data are treated differently. You've already been introduced to numbers, e.g. 2, and characters, e.g. "Hello". Notice that when R reads over any phrase in quotes, for example "Gators" or "2", it is interpreted as a character. To check data type, we use commands, class(). View the following example and guess the data type:

```{r}
number <- "4"
class(number)
```

Now let's see what happens if we try to add or multiply with our character 'number'.

```{r error=TRUE}
number <- "4"
number + 6
```

The error that arises is due to the incompatibility of the data types. Numeric data cannot be added to a character. A computer functions logically and there is no straightforward answer to a statement like "Chipotle Burrito" + 56. You can resolve this error in two ways: One is that we convert 'number' from a character to a numeric, using built-in function as.numeric().

```{r}
number <- "4"
number <- as.numeric(number)
class(number)
number + 6
```

Notice the use of the "\<-" operator to store the numeric output in the object number. Once number has been assigned a numeric value, adding 6 works as one would expect. Another solution is to redefine 'number'.

```{r}
number <- "4"
number <- 4
ten <- number+6
ten
```

The value assigned to the object number could also have been cleared with either the function remove() or rm().

```{r}
number <- "4"
remove(number)
number <- 4
ten <- number+6
ten
```

There are only two logical values: TRUE and FALSE. To be recognized as logical values, TRUE and FALSE must be in capital letters. If desired, we can abbreviate TRUE with T and FALSE with F. It's interesting to play with logical values when we convert them into numerics, you will find out that TRUE and FALSE can be treated as 1 and 0.

```{r}
as.numeric(TRUE)
as.numeric(F)
```

## Defining Vectors

Given that we have a clear idea about numbers and characters, let's work on vectors. (We will save Booleans after introducing vectors since there are multiple tricks to play them together.) Recall that vectors are combination of data in the same type, below is a vector with numbers:

```{r}
gpa <- c(4,5,4.5)
```

To concatenate or to combine 3 numbers into 1 vector, we use function c(). Now let's check "Environment" in the upper right partition if 'gpa' is properly created. There you can also see that "gpa" is a vector of "num", which stands for number(or numeric) with length of 3. The length of a vector means how many elements are included in the vector. You can also check the length of a vector using length().

```{r}
length(gpa)
```

You can also add elements to an existing vector using the concatenate function.

```{r}
gpa <- c(4,5,4.5)
gpa <- c(gpa,3)
length(gpa)
```

If the vector we want to generate has repeated elements, we can use function rep().

```{r}
rep(4,times=10)
rep(c(2,5), times=3) 
rep(c(2,5), each=3)      
```

Inside function rep(), we have different options: we can choose to repeat the whole vector for n times (times=n) or we can choose to repeat each element of the vector for n times (each=n).

~*Note that we are using "=" sign assigning values to options in functions, which cannot be replaced by arrows "\<-".*~

Let's look at an example of vectors with characters. This is common when we have people's name in the data set.

```{r}
name <- c("Alan","Jane","David","Smith")
class(name)
```

One important property of vectors is that they can only combine one type of data. Let's see what will happen if we combine numbers, logical values with characters.

```{r}
c(2,3,"David",TRUE)
```

If we combine different data types, all elements will be forced to characters.

## Dealing with missing values

In the real world, data is messy and rarely complete. Knowing how to handle missing values is an essential skill for anyone working with data. Consider the following example: there are 5 students in a class but the GPA of 1 is unknown. It is totally a different case when there are only 4 students in class. So, we need something like a placeholder, to tell R that there is an additional value even though I don't know what it is. In this case, we use "NA". Compare the following 2 cases:

```{r}
gpa <- c(4,5,4.5,3)
length(gpa)
```

```{r}
gpamissing <- c(4,5,4.5,3,NA)
length(gpamissing)
```

Notice that the length of gpamissing is 5, not 4. R still counts 'NA's as elements in the vector.

Missing values can be stored in a different way in some data. For example, if you read in a STATA data file to R and there you have missing value represented by ".". R will read it as characters, but if you want to convert into numbers, R will automatically fill in NA for objects that are not numbers.

```{r}
somedata <- c("3", "5", "2", ".", "1")    
as.numeric(somedata)
```

The warning here tells you that during conversion, there are objects that R cannot recognize. You need to check whether NA created correctly. The following case needs special attention:

```{r}
somedata2 <- c("3", "5", "2", ".", "1","3,5") 
as.numeric(somedata2)
```

Even though we know the last number is 3.5, but it is read in a format R cannot recognize, a NA is generated as well.

Why can we combine numbers with NA in a vector? NA can be treated as any data type, so it can be combined with any type of data without changing the type.

## Getting help in R

Now we have 2 vectors of numbers, gpa and gpamissing, and I want to explore the average GPA of all students, what should I do?

```{r}
mean(gpa)
mean(gpamissing)
```

Because we have NA in gpamissing, we cannot know the average of all 5 students. What if we still want to get the mean of the leftover 4 students, can we manipulate mean(gpamissing) in any way? In the case, include "?" in front of the function you want to get help with, or type "help()" with your objective function in the parenthesis. Let's check on the built-in function mean():

```{r}
?mean
help(mean)
```

You should now see a help page describing the inputs and outputs of the mean() function in the bottom left partition. There, we have a description about what this function is for and more importantly it shows options embedded in the function mean() as below.

![](mean_help.png){width="100%"}

One embedded option in function mean() is called trim, meaning the fraction of observations/data point to be trimmed from each end. For example, if we have outliers in data, we may assign trim=0.25 to get rid of 25% of the data points from both ends. Let's try it out using the following example:

```{r}
mean(gpa)
mean(x=gpa, trim=0.25)
```

Without the option trim, we are getting the mean of all 4 numbers. While after adding the trim option, we take out 25% of data points from both sides, which leave us only 4.5 and 5.

The other option is called na.rm, meaning that whether NA values should be stripped before calculating the mean. This is exactly what we want to get the mean score of the left 4 students. Let's include this option and calculate the result:

```{r}
mean(gpamissing)
mean(x=gpamissing, na.rm=TRUE)
mean(x=gpamissing, na.rm=T)
```

Now we got the mean without NAs! Also notice that we used logical values here for na.rm option.

~*Note that when you type in R script, if logical values are correctly recognized, they will change colors.*~

## Vector Indexing

Given a vector, we can select elements of the vector using index. In R, the first element of a vector is located at index 1, the second element at index 2 and so on. Indexing is done with [ ]:

```{r}
gpa <- c(4,5,4.5,3)
gpa[3]                  # Selects the 3rd element
gpa[3] <- 4.75          # Replaces the 3rd element with 4.75
gpa[2:4]                # Selects elements 2 to 4
gpa[c(1,4)]             # Selects elements 1 and 4
gpa[c(1,1,3)]           # Selects element 1 twice and element 3

```

~*Note that Parentheses () are for functions, while brackets [] are for indexing.*~

Indexing is quite useful when you try to add or delete elements in a vector.

```{r}
c(gpa[1],5,gpa[2:length(gpa)])   #add 5 between the 1st and 2nd element of gpa

```

```{r}
gpa[-1]      #get rid of the 1st element of x
gpa[-length(gpa)]      #get rid of the very last element of x
gpa[-c(1,3)]       #get rid of both 1st and 3rd elements of x
```

## Vector Operations

Your typical +, -, \*, /, and \^ all work with vectors

```{r}
extracredit <- c(0,0.25,0.5,0)
gpa+extracredit
gpa-extracredit
```

The + performs element-wise addition, and the - performs element-wise subtraction. You can even add vectors of varying lengths, but be extra wary of the results:

```{r}
gpa+c(0,0.25,0.5)
```

~*You got a warning from R! Warnings prints messages that R want you to pay attention.*~

~*Warnings are different from errors in the sense that errors are mistakes and will stop R from running, but warning won't.*~

Why do we get a warning? Because gpa had 4 elements but what we add had only 3, the addition operator exhausted the values of c(0,0.25,0.5) before returning to its first element to add to the 4th element of gpa. If any of these operations seem confusing, pay extra close attention to the environment partition as you run these code chunks. Keeping a close eye on the transformation of your data after each line of code is a habit that will prevent errors or worse incorrect results. You can also perform scalar addition and element-wise multiplication:

```{r}
gpa+0.5
```

Just as one might do math on paper, R follows order of operations:

```{r}
gpa*1.05
gpa+extracredit*0.5
```

It is also worth observing how operations affect missing values:

```{r}
gpamissing+1
gpamissing*1.05
```

Since we don't know the missing value, it's reasonable to say that we don't know what the missing value plus 1 will be.

## Vector Filtering

Filtering allows us to select part of a vector that meets a certain condition. We can select part of a vector using indexing, but what differs here is that we often don't know which element meets the condition. Here is when logical values come into play.

Before looking at how to filter, let's get to know logical values first.

```{r}
is_female1 <- c(FALSE,FALSE,TRUE,TRUE)
is_female2 <- c(F,F,T,T)
temp1 <- rep(FALSE, times=2)   
temp2 <- rep(TRUE, times=2)
is_female3 <- c(temp1,temp2)
```

How can we use these logical values for filtering? If the gpa vector we generated is exactly the gpas of these 4 students, whose gender is identified as before, how to use the logical values to select the gpa of female students?

```{r}
gpa[is_female1]
gpa[is_female1==T]  
```

Or if we want to get the gpa for male student only?

```{r}
gpa[is_female1==F]  
gpa[!is_female1] 
```

Exclamation mark ! can be interpreted as "not" and used to switch between TRUE and FALSE. Try the following:

```{r}
!TRUE      #not true = false
!FALSE     #not false = true
```

What we discussed above doesn't seem to differ much from using the index, since we still need to generate the vector of logical values with the knowledge of who is who. Luckily, there are some other ways to generate logical values, which are much more common when dealing with data.

You are likely familiar with less than '\<' and greater than or equal to '\>=' signs. Another operator you should know is the " == ". This is the same as asking R if the left side is equal to the right side. Try these out:

```{r}
2 == 1
5 > 3
4 <= 8
```

Why "=="? Because "=" is used to define objects, it will create confusions if we also use it to compare objects.

Now let's apply this method to filter gpa for students who have scores greater than 75:

```{r}
gpa > 4      #generate logical values that identify whether each student has a gpa >4
gpa[gpa>4]    #find gpas that are higher than 4
name[gpa>4]    #find student names that have gpa>4
```

Or we can also find the index of students who have gpa higher than 4:

```{r}
which(gpa>4)
```
which() is a very useful function in R, returning the index of the value which satisfies the given condition. Here which() returns 2 and 3, meaning that the 2nd and 3rd students in our data satisfied the condition that has a gpa higher than 4.


You can also add or combine conditions together to create a more specific filter. First observe how logical values interact:

```{r}
T&T 
T&F
F&F
c(T,T,F)&c(T,F,T)      
```

& here means that both conditions should be satisfied. Similar method applies if you have more than 2 conditions.

```{r}
T&F&T&T
```

Now lets apply this knowledge to find all female student names who have gpa>4:

```{r}
gpa>4 & is_female1
name[gpa>4 & is_female1]
```


You were able to combine conditions with the AND operator "&", you can also specify multiple conditions with the OR operator " \| ". Observe the following:

```{r}
T|T
T|F
F|F
c(T,T,F)|c(T,F,T)
```

'\|' here means that either one of the conditions should be satisfied. Similar method applies if we have more than 2 conditions.

Now lets use this knowledge to find the outliers in the scores. You can use the OR operator '\|' to extract the student name who has a gpa equals to 5 or lower than 3.5:

```{r}
gpa==5 | gpa<3.5
name[gpa==5 | gpa<3.5]
```

## Conclusion

You've learned about commenting, data types, defining objects and vectors, vector operations and indexing, filtering, and troubleshooting in today's class. With these basics and a healthy use of the help() function, there is no error you can't solve!
